## 6/26
Discussing anf Finalizing wire framing - Discussed best options between MongoDb and SQL
## 6/27
Taking next steps in Auth coding, discussing how code will look with SQL. Beginning to code BE. (End of Day update) Finished Back End Auth - need to add personalized project data
## 6/28
Finished Auth last night. Discussing objectives for today TBD - We will be working on backend FastAPI - Foreign Key blockers
## 6/29
Adding additions CRUD backend methods today routers and accounts - Continuing to mob code - No blockers - End of day update Finished CRUD - some bug issues on update function. Finished delete, post, get_all, get_list endpoints. Working on update
## 6/30
Finished CRUD yesterday. Update needs to be finished, some debugging blockers around Update. Objective for the day is to finish CRUD and start on the FE
## 7/10
Discussing next Steps for Front End - Started Coding Front End - Goals to complete are Home, APP, Nav, SignUp and LogIn pages on react. Only Blockers are Auth for FE
## 7/11
Debugging Auth Front End blockers having issues when logging in. We are coding independently for the first half of the day to see if we can come up with a solution.
## 7/12
Mob coding this morning and co programming - Coded out Nav Js for front end. We are still running into issues with error handling with duplicate user names. Will move forward with front end and continue working on FE and solutions for blockers
## 7/13
We have decided to implement an email feature for create dogs. We need to go back to the backend and implement this function. We will mob program the back end together. Next we will continue with the front end features. Delete, Edit and List all and my dogs.
## 7/14
Today we wrapped up Delete and Edit functions for front end. We are making great progress and have completed the front end. We are going to implement testing starting Monday
## 7/17
Today we are focusing on Testing. Each of us will be writing a test for the project
## 7/18
Starting the day with testing our functions. All tests have successfully passed. We will now be moving on a navigate function for edit pages and changing Dogout ID to an int.
## 7/19
Mob coding today to fix a data error for time.sleep on the login and Signup function. issues fixed this morning. We will be cleaning up the code as well today. Discussing next issues. Finished MVP
## 7/20
Finished MVP yesterday. Playing with application tyo find any bugs. Steadying for final until deployment lecture. 
