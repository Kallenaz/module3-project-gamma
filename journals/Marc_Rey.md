June 26, 2023 (Day 1) -
A lot of prep today for the upcoming project, mapped our MVP to have a clear idea of what we want to do.

June 27, 2023 (Day 2) -
Starting Auth and attempting to finish, then continue on from there.

June 28, 2023, (Day 3) -
Finished Auth now need to start forming the pages for front end as well as some more things for back end regarding the personalized information.

June 29, 2023, (Day 4) -
Working on finishing the rest of the backend and then contuing on to Friday!

June 30, 2023, (Day 5) -
Finishing mob coding for backend specifically backend CRUD, small debugging but moving on to frontend afterwards

July 10, 2023, (Day 6) -
Started coding the frontend, added Login, Home, Signup, and App so far. Going to continue the rest of the day added the baseline/outline of the frontend so that we can work on authentication afterwards. Blockers we have are figuring out the authentication for the frontend, the backend auth works and the information is easily processed and displayed..

July 11, 2023 (Day 6) -
Adding frontend authenticator with jwtdown react, currently having a blocker regarding that

July 12, 2023 (Day 7) -
Going to continue on working on the frontend authenticator blocker, then going to continue to make the dog list as well as the nav.js

July 13th, 2023 (Day 8) -
We completed login and signup forms yesterday. We are going to continue working on the frontend for the dog form and list dog page. We will work on the backend table so that email and other contact information is able to be displayed.

July 14th, 2023 (Day 9)
Finishing up the my dogs delete features as well as button functionality, customization soon.

July 17th, 2023 (Day 10) -
We debugged some issues with flake8 and are going to focus mainly today on creating tests.

July 18th, 2023 (Day 11) -
Had to fix codes to fix routers and queries in order for the backend frontend functionality to work. All 5 members of the group tests work and still looking to customize later down the road.

July 19th, 2023 (Day 12) -
Fixed up error handling as well as a problem where if a new user signs up with the same username and same password it logs them in, as well as fixed the timing of the code so that it matches the fetch token so that there will not be a flash of "account already made" then logs in.

July 20th, 2023 (Day 13) -
Finished up MVP for the project and potentially looking to work on stretchgoals, going to study for upcoming exam and then look back into it the following week!
