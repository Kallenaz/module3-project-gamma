## July 20, 2023
The README was added and completed. We are finished as far as project requirements go. Everything is functional and flake8 was ran one last time. Any console logs that were used for development were removed. All we have left is to deploy the application. We're going to use the next couple of days to prepare for the assessment. Our instructor is going to walk everyone through deployment later next week. For now I'll add dogs to my database. We discussed adding a stretch goals branch for anyone who wants to add any styling. It would be nice to see our color scheme and a logo added before the module is over. Adding to the home page would be another stretch goal I have, right now there is nothing on it so there is a lot of room to play with there.


## July 19, 2023
Login and Signup is functional without the flashing bug. We added a timeout function to prevent the message from showing. Error Handling is complete, we could add more, but it is not necessary. Today's focus is cleaning up the front-end and removing any extra console.logs from development.


## July 18, 2023
Our account data was not being used in our dogs queries so we integrated account data id with the rehomer id. We all changed our code for our assigned test route. Then we added our test functions and tested them out one by one. All five tests are passing now. Next we need to start working in the front end to make our pages look more presentable.


## July 17, 2023
After today's lecture we went through the flake8 errors and resolved those. Our focus now is writing tests, we will each be writing a test. I am responsible for writing the my_dogs test.


## July 14, 2023
We're almost done coding out the front-end. What's left is the my dogs page and the delete feature. The delete and edit functionality will be added as buttons to the my dogs page. That is our goal for today, if need be we'll finish up monday before starting any customization.


## July 13, 2023
We need to go back to the back-end to add to the dog table. We are going to add email to the dog card so users are able to contact the owner. After we update the table finishing the create dog and list dog page is the next priority.


## July 12, 2023
Right now we're dealing with a blocker in logging in and signing up without duplicates. We will spend time researching this tonight and continue to mob code in the front end. Our goal is to start the list dogs dogs and create dog today.


## July 11, 2023
Front-end authentication is finished. We decided to continue using react and not go with redux. Tomorrow we plan on working in teams to develop the list view and create dog view.


## July 10, 2023
The front-end has a good template for a home, app, nav, signup, and login. Tomorrow we will start front-end authentication. Currently we're ahead of schedule and brainstorming what we need to accomplish next.


## June 30, 2023
We are taking the day to catch up on explorations since we are at a good stopping point. Summer break will be from July 3-7.


## June 29, 2023
We're almost finished with our backend. We made progress as a team on the CRUD operations for the dog table in the routers and queries. There were a few erors we ran into and were able to fix most. Update dog is still a work in progress.


## June 28, 2023
Our next goal is to finish setting up the tables for our database and creating queries and routers for each. We ran into an issue with connecting the account_id to the rehomer_id. We had to individually delete our tables in the database and pull in the new tables to fix this. We are still looking into why it may not be connecting. On a positive note, we were able to add the DuplicateAccountError HTTPException to the create account function resulting in a 400 if that were to occur.

## June 27, 2023
We successfully completed our backend without personalized data, following along Curtis' video and taking turns coding. The wireframe, api design and journals have been added to the repository. Our database is set up and we are connected to both fastapi and react. We are on schedule and did a great job communicating throughout our first day coding.


## June 26, 2023
First day with the repository and we organized our thoughts on our trello board. Today we focused on API endpoints and watching the JWTdown FastApi video. We decided on going with PostgreSQL for our database and to begin coding authentication tomorrow. Overall we're more than prepared and I feel confident in our ability to work as a team. We plan on taking turns every thirty minutes to code for the back end. Once we get to the front end we will go into pairs and work on feature branches. We agreed on using the Gong method to annouce pushing and pulling, hoping to minimize merge conflicts and stay up-to-date with the code.